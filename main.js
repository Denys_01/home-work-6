
function createNewUser(){
    const name = prompt("Write your name :");
    const surname = prompt("Write your surname :");
    let dateUser = prompt("Write birthday dd.mm.yyyy");//dd.mm.yyyy
    let dateNow = new Date();
    let year = "";
    let month = "";
    let day = "";
    for(let i = 0; i < dateUser.length;i++){
        if(i < 2 && i != 2){
            day += dateUser[i];
        }else if( i > 2 && i < 5 && i != 5){
            month +=dateUser[i];
        }else if( i > 5 ){
            year +=dateUser[i];
        }
    }
    let dateUser1 = new Date(+year,+month,+day); 
    const newUser = {
        firstName : name,
        lastName : surname,
        getLogin : function(){
            const login =this.firstName[0]+this.lastName;
            return login.toLowerCase();
        },
        setFirstName : function(){
            const newFirstName = prompt("Write new name : ");
            this.firstName = newFirstName;
        },
        setLastName : function(){
            const  newLastName = prompt("Write new surname : ");
            this.lastName = newLastName;
        },
        getAge : function(){
            let date = dateNow.getFullYear()-dateUser1.getFullYear();
            return date;
        },
        getPassword : function(){
            let password = this.firstName[0].toUpperCase() + 
                this.lastName.toLowerCase() + dateUser1.getFullYear();
            return password;
        }

    }
    return newUser;
}
let user = createNewUser();
console.log(user.getAge());
console.log(user.getPassword());